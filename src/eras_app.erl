%%%------------------------------------------------------------------------
%%% @author zeb <zebbey@gmail.com>
%%% @copyright zeb
%%% @doc
%%% 认证服务器应用程序
%%% @end
%%%------------------------------------------------------------------------

-module(eras_app).

-behaviour(application).

-export([
	 start/0,
	 start/2,
	 stop/1
	]).

start() ->
    application:start(eras).

start(_StartType, _StartArgs) ->
    eras_sup:start_link().

stop(_State) ->
    ok.
