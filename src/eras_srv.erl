%%%------------------------------------------------------------------------
%%% @author zeb <zebbey@gmail.com>
%%% @copyright zeb
%%% @doc
%%% 认证服务进程
%%% @end
%%%------------------------------------------------------------------------

-module(eras_srv).
-behaviour(gen_server).
-include_lib("xmerl/include/xmerl.hrl").

%% API
-export([
	 start_link/0,
	 authenticate/2
 	]).

%% gen_server callbacks
-export([
	 init/1,
	 handle_call/3,
	 handle_cast/2,
	 handle_info/2,
	 terminate/2,
	 code_change/3
	]).

%% internal

%%%================================================================
%%% API
%%%================================================================
start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

authenticate(Account, Passwd) ->
    gen_server:call(?MODULE, {auth, Account, Passwd}).

%%%================================================================
%%% gen_server callbacks
%%%================================================================

init([]) ->
    {ok, null}.

handle_call({auth, Account, Passwd}, From, State) ->
    spawn_link(fun() -> do_auth(From, Account, Passwd) end),
    {noreply, State};
    
handle_call(_Msg, _From, State) ->
    {reply, ok, State}.

handle_cast(_Cast, State) ->
    {noreply, State}.

handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVersion, State, _Extra) ->
    {ok, State}.

%%%================================================================
%%% internal
%%%================================================================
do_auth(From, Account, Passwd) ->
    gen_server:reply(From, ok).

ado_auth(From, Account, Passwd) ->
    Url = string:join(["http://xxx?account=", Account, "&pwd=", Passwd], ""),
    {ok, {_Head, _Param, Content}} = httpc:request(Url),
    {Doc, _} = xmerl_scan:string(Content),
    [#xmlElement{content = [#xmlText{value = Id}|_]}|_] = xmerl_xpath:string("/xml/userid", Doc),
    {UserId, _} = string:to_integer(Id),
    case UserId of
	_ when UserId > 0 ->
	    io:format("auth ok~n"),
	    gen_server:reply(From, ok);
	_ ->
	    io:format("auth failed~n"),
	    gen_server:reply(From, failed)
    end.
