%%%------------------------------------------------------------------------
%%% @author zeb <zebbey@gmail.com>
%%% @copyright zeb
%%% @doc
%%% 认证服务器顶层监控进程
%%% @end
%%%------------------------------------------------------------------------

-module(eras_sup).
-behaviour(supervisor).

-export([start_link/0]).

-export([init/1]).

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

init([]) ->
    RestartStrategy = one_for_one,
    AllowedRestarts = 10,
    MaxSeconds = 30,

    SupFlags = {RestartStrategy, AllowedRestarts, MaxSeconds},
    Restart = permanent,
    Shutdown = 2000,
    
    AuthSrv = {eras_srv, {eras_srv, start_link, []}, Restart, Shutdown, worker, [eras_srv]},

    Children = [AuthSrv],
    
    {ok, {SupFlags, Children}}.


